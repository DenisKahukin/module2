﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dimensions dimensions = new Dimensions();
            Program prog = new Program();
            prog.GetTotalTax(10, 500, 5);
            prog.GetCongratulation(19);
            prog.GetMultipliedNumbers("132","2");
            prog.GetFigureValues(FigureEnum.Circle, ParameterEnum.Square, dimensions);
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            if (companiesNumber < 0 || tax < 0 || companyRevenue < 0) 
            {
                throw new IndexOutOfRangeException();
            }
            double percent = (double)companyRevenue / 100;
            return Convert.ToInt32(companiesNumber * tax * percent);
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {
                return "Поздравляю с совершеннолетием!";
            }
            if(input %2 !=0 && input > 12 && input < 18)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            if(input <= 0)
            {
                throw new ArgumentException();
            }
            return $"Поздравляю с {input}-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            if (first.Contains(',') || second.Contains(','))
            {
                NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
                CultureInfo culture = CultureInfo.CreateSpecificCulture("ru-RU");
                if (Double.TryParse(first, style, culture, out double firstNumber) && Double.TryParse(second, out double secondNumber))
                {
                    return (double)firstNumber * secondNumber;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            if (first.Contains('.') || second.Contains('.'))
            {
                NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-EN");
                if (Double.TryParse(first, style, culture, out double firstNumber) && Double.TryParse(second, out double secondNumber))
                {
                    return firstNumber * secondNumber;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            if (Double.TryParse(first, out double firstNumbers) && Double.TryParse(second, out double secondNumbers))
            {
                return firstNumbers * secondNumbers;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            switch (figureType)
            {
                case FigureEnum.Triangle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            return Math.Truncate(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                        case ParameterEnum.Square:
                            if(dimensions.Height == 0)
                            {
                                double halfPerimetr = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                return Math.Truncate(Math.Sqrt(halfPerimetr * (halfPerimetr - dimensions.FirstSide) * (halfPerimetr - dimensions.SecondSide) * (halfPerimetr - dimensions.ThirdSide)));
                            }
                            return Math.Truncate(0.5 * dimensions.Height * dimensions.FirstSide);   
                    }
                    break;
                case FigureEnum.Rectangle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            return Math.Truncate((dimensions.FirstSide + dimensions.SecondSide) * 2);
                        case ParameterEnum.Square:
                            return Math.Truncate(dimensions.FirstSide * dimensions.SecondSide);
                       
                    }
                    break;
                case FigureEnum.Circle:
                    switch (parameterToCompute)
                    {
                        case ParameterEnum.Perimeter:
                            return Math.Floor(2 * Math.PI * dimensions.Radius);
                        case ParameterEnum.Square:
                            if(dimensions.Radius == 0)
                            {
                                return Math.Floor((Math.PI * Math.Pow(50, 2)) / 4);
                            }
                            return Math.Ceiling(Math.PI * Math.Pow(dimensions.Radius, 2));
                    }
                    break;
            }
            throw new ArgumentException();
        }        
    }
}
